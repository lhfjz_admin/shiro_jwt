package com.example.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/order")
public class OrderController {

    @RequestMapping("/find")
    @RequiresPermissions("order:find:*") //基于权限字符串的访问控制
    @ResponseBody
    public String find(){
        System.out.println("以访问到find方法");
        return "以访问到find方法";
    }

    @RequestMapping("/add")
    @RequiresRoles("super") //基于多角色的权限控制
    @ResponseBody
    public String add(){
        System.out.println("以访问到add方法");
        return "以访问到add方法";
    }
}
