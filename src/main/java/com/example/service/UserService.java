package com.example.service;

import com.example.entity.Perms;
import com.example.entity.User;

import java.util.List;

public interface UserService {
    /**
     * 注册用户
     * @param user
     */
    void register(User user);

    /**
     * 根据用户名查找用户
     * @param name
     * @return
     */
    User findUserByname(String name);

    User findRolesByUserName(String name);

    List<Perms> findPermsByRoleId(String id);
}
