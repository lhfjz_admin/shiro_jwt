package com.example.service.impl;

import com.example.dao.UserDao;
import com.example.entity.Perms;
import com.example.entity.User;
import com.example.service.UserService;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.utils.SaltUtil.getSalt;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public void register(User user) {
        String salt = getSalt(4);
        Md5Hash md5Hash=new Md5Hash(user.getPassword(),salt,1024);
        user.setPassword(md5Hash.toString());
        user.setSalt(salt);
        userDao.save(user);
    }

    @Override
    public User findUserByname(String name) {
        User userByName = userDao.findUserByName(name);
        return userByName;
    }

    @Override
    public User findRolesByUserName(String name) {
        return userDao.findRolesByUserName(name);
    }

    @Override
    public List<Perms> findPermsByRoleId(String id) {
        return userDao.findPermsByRoleId(id);
    }
}
