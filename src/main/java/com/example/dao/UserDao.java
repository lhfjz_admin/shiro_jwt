package com.example.dao;

import com.example.entity.Perms;
import com.example.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDao {
    int save(User user);
    User findUserByName(String name);
    //根据用户名查询所有角色
    User findRolesByUserName(String name);
    //根据用户名查询权限集合
    List<Perms> findPermsByRoleId(String id);
}
